---
title       : die Uni
subtitle    : Aufbau, Funktionsweise, Hilfestellungen
framework   : io2012        # {io2012, html5slides, shower, dzslides, ...}
highlighter : highlight.js  # {highlight.js, prettify, highlight}
hitheme     : tomorrow      #
widgets     : []            # {mathjax, quiz, bootstrap}
mode        : selfcontained # {standalone, draft}
---

## Hilfe zur Selbsthilfe

* Fragen in der Vorlesung und Tutorien stellen
(was ihr nicht verstanden habt, haben vermutlich auch 20 weitere Menschen nicht verstanden)
* Helft euch gegenseitig
* Mentoring-Programm

---

## Beratungsangebote:

* FSI: http://fsi.spline.de/beratung/
* Prüfungsbüro: http://www.imp.fu-berlin.de/fbv/pruefungsbuero/index.html
* durch ProfessorInnen: https://www.mi.fu-berlin.de/pbs/advisors.faces
* AStA: http://www.astafu.de/beratungen
* Allgemeine psychologische und Studienberatung
    * http://www.fu-berlin.de/studium/beratung/allgemeine-studienberatung/index.html
    * http://www.fu-berlin.de/studium/beratung/psychologische-beratung/index.html

---

* Beratung für Menschen mit Behinderungen oder chronischen Erkrankungen:
    * http://www.fu-berlin.de/einrichtungen/service/studierende/behinderte/index.html
* Familienbüro: http://www.fu-berlin.de/sites/familienbuero/index.html
* Career Service: http://www.fu-berlin.de/sites/career/index.html
* Internationale Studierende:
    * http://www.fu-berlin.de/sites/studienberatung/info-service/ISFU/index.html

---

## Wie ist die Uni aufgebaut?

* 11 Fachbereiche
(z.B. Geowissenschaften; Geschichts- und Kulturwissenschaften; Mathematik und Informatik; ...)
* die Charité
* 3 Zentralinstitute
(Lateinamerika-Institut, Osteuropa-Institut, John-F.-Kennedy-Institut)

* die Fachbereiche gliedern sich in Institute:
z.B. Institut für Informatik, Institut für Mathematik

---

## Wie werden an der Uni Entscheidungen getroffen?
### Akademische Selbstverwaltung

Auf jeder Ebene gibt es sowas wie eine Exekutive und eine Legislative:
* Universität: Präsidium + Akademischer Senat
* Fachbereich: Dekanat + Fachbereichsrat
* Institut: GeschäftsführerIn + Institutsrat

Besetzt durch 50% + 1 ProfessorIn, Rest: 1/3 Studierende,
1/3 Wissenschaftliche MitarbeiterInnen, 1/3 Sonstige MitarbeiterInnen

Alle Gremien tagen öffentlich!

---

Zusätzlich gibt es zu den verschiedenen Gremien oftmals (Experten-)Kommissionen mit beratendem Charakter und verschiedene Ausschüsse:

* auf Fachbereichsebene:
    * der Prüfungsausschuss (Anerkennung von Prüfungsleistungen)
    * die Kommission für Ausbildung und neue Studiengänge
    * ...

* auf Uniebene
    * Kommission für Lehre
    * Kommission für Forschung
    * ...

---

## Was gibt es zu entscheiden?

* Uni-Ebene (Akademischer Senat, 13/4/4/4):
    * Einrichtung neuer Studiengänge, Institute,… (oder Schließung dieser)
    * Umstellung auf Bachelor/Master.
    * Einrichtung des Campus Management Systems.
* Fachbereichsebene (Fachbereichsrat 7/2/2/2):
    * Angelegenheiten von Lehre, Studium und Qualifikation,
    z.B. Änderung von Studien- und Prüfungsordnungen.
    * Berufung neuer ProfessorInnen.

---

* Institutsebene (Institutsrat 4/1/1/1):
    * Lehrplan.
    * Rauchverbot für die Innenhöfe.
    * Neue Schließanlage.
    * das Loch an der Decke im Keller zumachen.

---

## Die studentische Selbstverwaltung

* Legislative: Studierendenparlament (StuPa)
    * 60 Mitglieder von euch jährlich im Januar gewählt.
    * Grundsätzliche Angelegenheiten der Studierendenschaft: Resolutionen
    * Festlegung eines Haushalts
    * Festlegung einer Wahlordnung, Satzung
* Exekutive: Allgemeiner Studierendenausschuss (AStA)
    * 13 Referate
    * Unterstützung studentischer Initiativen
    * umfangreiches Beratungsangebot
* Zusätzlich: Studentische Vollversammlung
    * Entscheidungen haben "empfehlenden Charakter".
* auf Fachbereichsebene: Fachschaftsinitiativen

---

## Wo gibt es Infos?

* Seiten der FSIen:
    * http://fsi.spline.de
    * http://fsi.spline.de/mathe/
* AStA im Netz:
    * http://www.astafu.de/
* Studentischer Wahlvorstand:
    * http://www.fu-berlin.de/sites/studwv/
* Protokolle aus dem Akademischen Senat (nur intern):
    * http://www.fu-berlin.de/service/zuvdocs/senat/protokolle/index.html
* Das Berliner Hochschulgesetz
    * http://gesetze.berlin.de/default.aspx?WORDS=BerlHG&BTSEARCH.X=42
* Die Rahmenstudien- und -prüfungsordnung (RSPO)
    * http://www.fu-berlin.de/service/zuvdocs/amtsblatt/2013/ab322013.pdf?1377608774

---

## Wie kann ich selbst aktiv werden?

* Wählen gehen!
* Bei der FSI mitmachen :)
* Oder bei spline
* Oder eigene tolle Initiative gründen
* Mitmachen!


