# Unistruktur

This repository contains a talk on the university's structures, gremia and university politics. Stand 2013.

## to view the presentation

Ohm.. on github, it gets deployed on github pages.. Otherwise you can view it locally (with or without webserver)...

## to build the slides

* checkout this repo
* install [slidify](https://github.com/ramnathv/slidify)
* use the `make.r` script

## license

This talk is licensed under the Creative Commons Attribution-ShareAlike 3.0 (CC BY-SA 3.0) License. For more detailed information, visit [https://creativecommons.org/licenses/by-sa/3.0/](https://creativecommons.org/licenses/by-sa/3.0/)
